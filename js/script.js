const ruta = './json/data.json'; //Ruta del archivo
var datos_estudiantes = document.querySelector('#datos-estudiantes');
var alerta = document.querySelector('#alerta');
let flag = false; //controlar los innerHTML

document.getElementById("btn-leer").addEventListener("click", function() {
    leerDatos(ruta);
});

function leerDatos(datoRuta){
    fetch(datoRuta) //Leer el archivo de la ruta
    .then(respuesta => respuesta.json()) //Formato de obtención de los datos
    .then(respuesta => {mostrar(respuesta)})
    .catch(error => console.log(error)); //Mostrar los errores por consola
}

//Mostrar los datos de los estudiantes
function mostrar(datos){
    if (flag == false){
        for(let valor of datos){
            datos_estudiantes.innerHTML += 
                `
                    <tr>
                        <td>${valor.cedula}</td>
                        <td>${valor.nombre}</td>
                        <td>${valor.direccion}</td>
                        <td>${valor.telefono}</td>
                        <td>${valor.correo}</td>
                        <td>${valor.curso}</td>
                        <td>${valor.paralelo}</td>
                    </tr>
                `;
        }
        flag = true;
    } else {
        alerta.innerHTML = 
            `
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Error!</strong> Los datos ya fueron leídos.
                </div>
            `;
    }
}